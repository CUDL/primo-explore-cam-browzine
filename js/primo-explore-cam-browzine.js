var app = angular.module('viewCustom', ['angularLoad', 'searchResult']);

class BrowzineController {
  constructor(browzineService, $scope) {
    this.browzineService = browzineService;
    this.$scope = $scope;
  }

  $onInit() {
    this.browzineService.handleSearchResult(this.$scope);
  }
}

BrowzineController.$inject = ['browzineService', '$scope'];

var BrowzineComponent = {
  require: {
    parentCtrl: '^prmSearchResultAvailabilityLine',
  },
  controller: BrowzineController,
};

const configure = (window) => {
  window.browzine = {
	  api: "https://public-api.thirdiron.com/public/v1/libraries/603",
	  apiKey: "9dc49934-bcac-4cf4-8d41-e9ada3871e93",

	  journalCoverImagesEnabled: true,

	  journalBrowZineWebLinkTextEnabled: true,
	  journalBrowZineWebLinkText: "View Journal Contents",

	  articleBrowZineWebLinkTextEnabled: true,
	  articleBrowZineWebLinkText: "View Issue Contents",

	  articlePDFDownloadLinkEnabled: true,
	  articlePDFDownloadLinkText: "Download PDF",

	  articleLinkEnabled: true,
	  articleLinkText: "Read Article",

	  printRecordsIntegrationEnabled: true,

	  unpaywallEmailAddressKey: "unpaywall@lib.cam.ac.uk",

	  articlePDFDownloadViaUnpaywallEnabled: true,
	  articlePDFDownloadViaUnpaywallText: "Download PDF (via Unpaywall)",

	  articleLinkViaUnpaywallEnabled: true,
	  articleLinkViaUnpaywallText: "Read Article (via Unpaywall)",

	  articleAcceptedManuscriptPDFViaUnpaywallEnabled: true,
	  articleAcceptedManuscriptPDFViaUnpaywallText: "Download PDF (Accepted Manuscript via Unpaywall)",

	  articleAcceptedManuscriptArticleLinkViaUnpaywallEnabled: true,
	  articleAcceptedManuscriptArticleLinkViaUnpaywallText: "Read Article (Accepted Manuscript via Unpaywall)"
	};
};

const loadScript = (window) => {
  const script = window.document.createElement('script');
  script.src =
    'https://s3.amazonaws.com/browzine-adapters/primo/browzine-primo-adapter.js';
  window.document.head.appendChild(script);
};

class BrowzineService {
  constructor($window) {
    this.$window = $window;
  }

  /**
   * To be called in the module's run block.
   */
  init() {
    configure(this.$window);
    loadScript(this.$window);
  }

  /**
   * This is where the Browzine/LibKey integration does its magic.
   *
   * @param {Object} scope - An AngularJS $scope object. The scope is assumed
   * to belong to a PrmSearchAvailabilityLineAfter component, with a parentCtrl
   * property.
   */
  handleSearchResult(scope) {
    this.$window.browzine.primo.searchResult(scope);
  }
}


BrowzineService.$inject = ['$window'];

// NB test - based on HathiTrust
angular
  .module('npldLink', [])
  .controller('npldLinkController', [function() {
    var self = this;

    self.$onInit = function() {
      self.getDocid = self.parentCtrl.result.pnx.control.recordid[0];
      self.isNpld = !!self.getDocid.match(/^44CAM_NPLD/);
      // JP NOTE: I'm not sure how this will be used, so I'll just log it here...
      console.log(self.getDocid + (self.isNpld ? ' is ' : ' is not ') + 'a NPLD ID');
    };
  }])
  .component('npldLink', {
    require: {
      parentCtrl: '^prmSearchResultAvailabilityLine',
    },
    controller: 'npldLinkController',
    template: '<span ng-if="$ctrl.isNpld" id="legal-deposit-explanation">UK legislation regulates access - <a href="https://libguides.cam.ac.uk/e-legal-deposit" id="legal-deposit-link" target="_blank">why?</a></span>'
    });

angular
  .module('searchResult', ['npldLink'])
	.service('browzineService', BrowzineService)
	.component('browzine', BrowzineComponent)
	.component('prmSearchResultAvailabilityLineAfter', {template: '<browzine></browzine><npld-link></npld-link>'})
	.run(['browzineService', browzineService => browzineService.init()]);
